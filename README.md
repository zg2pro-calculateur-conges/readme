# calculateur-jours-conges

| Nest        | Vue           | Angular  |
| ------------- |:-------------:| -----:|
|  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>      | <a href="https://vuejs.org/" target="blank"><img src="https://vuejs.org/images/logo.png" width="320" alt="Vue Logo" /></a> | <a href="https://angular.io/" target="blank"><img src="https://angular.io/assets/images/logos/angular/angular.svg" width="320" alt="Angular Logo" /></a> |

https://www.npmjs.com/package/@zg2pro-org/calculateur-conges-api

https://zg2pro-calculateur-conges.gitlab.io/calculateur-conges-vue/

https://zg2pro-calculateur-conges.gitlab.io/calculateur-conges-angular/


I've used npm-registry from npmjs.org (the central npmregistry). To use it, you have to create an organization, use this organization as a groupId for your libs, and publish to this org.

The purpose of this PoC is to show that we can, thanks to Nest JS, extract the business logic of applications in APIs,
that they can be integrated in all frameworks, I will show it with a vuejs, with angular, but also with server-side frameworks (behind a spring-boot mvc),
it would obviously be possible with python, php etc ... And thanks to this api-zation not only I allow my framework
to become modular with all the gain that API-zation represents, but in addition I gain by allowing my technologies to be
front or back, to update quickly, or even to be able to interchange (I pass my code from an angular framework to
a react or svelte framework, I go from java to serverless lambdas, I go from php to an expressjs server, simply, and I can
even transfer a code from the front to the back OR, and it's much better, from the back to the front!

To do this all, follow the instructions in the sub-READMEs :

- check the projects in groups: https://gitlab.com/zg2pro-calculateur-conges
